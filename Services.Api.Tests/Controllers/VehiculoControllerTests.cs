﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Api.Company.Controllers;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Services.Api.Company.Core.Entities;
using Services.Api.Company.Core.Mapper;
using Services.Api.Company.Core.Repository.VehiculoRepository;
using Services.Api.Company.Core.Services.VehiculoService;
using Services.Api.Company.Models;

namespace Services.Api.Company.Controllers.Tests
{
    [TestClass()]
    public class VehiculoControllerTests
    {
        private IServiceProvider serviceProvider;

        [TestInitialize]
        public void Initialize()
        {
            ServiceCollection services = new ServiceCollection();

            services.AddDbContext<PruebaTecnicaKonicaContext>();

            services.AddScoped<VehiculoController>();

            services.AddScoped<IVehiculoRepository, VehiculoRepository>();
            services.AddScoped<IVehiculoService, VehiculoService>();

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new VehiculoMappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc();

            serviceProvider = services.BuildServiceProvider();
        }

        [TestMethod()]
        public void GetAllTest()
        {
            var controller = serviceProvider.GetService<VehiculoController>();

            Assert.IsNotNull(controller);

            var getAllResponse = controller.GetAll().Result as OkObjectResult;

            Assert.IsNotNull(getAllResponse);

            Assert.AreEqual(200, getAllResponse.StatusCode);
        }

        [TestMethod()]
        public void GetTest()
        {
            var controller = serviceProvider.GetService<VehiculoController>();

            Assert.IsNotNull(controller);

            var getResponse = controller.Get(1).Result as OkObjectResult;

            Assert.IsNotNull(getResponse);

            Assert.AreEqual(200, getResponse.StatusCode);

            var vehiculoObtenido = getResponse.Value as VehiculoModel;

            Assert.IsNotNull(vehiculoObtenido);

            Assert.AreEqual("Toyota", vehiculoObtenido.Descripcion);
        }
    }
}