﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Services.Api.Company.Core.Entities;
using Services.Api.Company.Core.Mapper;
using Services.Api.Company.Core.Repository.PosicionRepository;
using Services.Api.Company.Core.Services.PosicionService;
using Services.Api.Company.Models;

namespace Services.Api.Company.Controllers.Tests
{
    [TestClass()]
    public class PosicionControllerTests
    {
        private IServiceProvider serviceProvider;

        [TestInitialize]
        public void Initialize()
        {
            ServiceCollection services = new ServiceCollection();

            services.AddDbContext<PruebaTecnicaKonicaContext>();

            services.AddScoped<PosicionController>();

            services.AddScoped<IPosicionRepository, PosicionRepository>();
            services.AddScoped<IPosicionService, PosicionService>();

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new PosicionMappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc();

            serviceProvider = services.BuildServiceProvider();
        }

        [TestMethod()]
        public void PostAddPosicionTest()
        {
            var controller = serviceProvider.GetService<PosicionController>();

            Assert.IsNotNull(controller);

            PosicionModel nuevaPosicion = new PosicionModel
            {
                Ubicacion = "38.7197683320907, -9.135730131435745",
                VehiculoId = 4
            };

            var getResponse = controller.PostAddPosicion(nuevaPosicion).Result as OkObjectResult;

            Assert.IsNotNull(getResponse);

            Assert.AreEqual(200, getResponse.StatusCode);

            var posicionCreada = getResponse.Value as PosicionModel;

            Assert.IsNotNull(posicionCreada);

            Assert.AreEqual(nuevaPosicion.Ubicacion, posicionCreada.Ubicacion);

            Assert.AreEqual(nuevaPosicion.VehiculoId, posicionCreada.VehiculoId);
        }

        [TestMethod()]
        public void PostGetAllPosicionesByVehiculoIdTest()
        {
            var controller = serviceProvider.GetService<PosicionController>();

            Assert.IsNotNull(controller);

            var getAllResponse = controller.PostGetAllPosicionesByVehiculoId(8).Result as OkObjectResult;

            Assert.IsNotNull(getAllResponse);

            Assert.AreEqual(200, getAllResponse.StatusCode);
        }

        [TestMethod()]
        public void PostUltimaPosicionTest()
        {
            #region Creación de una posición

            var controller = serviceProvider.GetService<PosicionController>();

            Assert.IsNotNull(controller);

            PosicionModel nuevaPosicion = new PosicionModel
            {
                Ubicacion = "43.361513395661106, -8.41113235517224",
                VehiculoId = 6
            };

            var getResponse = controller.PostAddPosicion(nuevaPosicion).Result as OkObjectResult;

            Assert.IsNotNull(getResponse);

            Assert.AreEqual(200, getResponse.StatusCode);

            var posicionCreada = getResponse.Value as PosicionModel;

            Assert.IsNotNull(posicionCreada);

            Assert.AreEqual(nuevaPosicion.Ubicacion, posicionCreada.Ubicacion);

            Assert.AreEqual(nuevaPosicion.VehiculoId, posicionCreada.VehiculoId);

            #endregion

            #region Obtención última posición

            var getResponseUltimaPosicion = controller.PostUltimaPosicion(nuevaPosicion.VehiculoId).Result as OkObjectResult;

            Assert.IsNotNull(getResponseUltimaPosicion);

            Assert.AreEqual(200, getResponseUltimaPosicion.StatusCode);

            var posicionObtenida = getResponseUltimaPosicion.Value as PosicionModel;

            Assert.IsNotNull(posicionObtenida);

            Assert.AreEqual(posicionCreada.Id, posicionObtenida.Id);

            #endregion
        }
    }
}