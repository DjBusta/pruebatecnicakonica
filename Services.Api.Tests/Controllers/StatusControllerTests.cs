﻿using Microsoft.AspNetCore.Mvc;
using Services.Api.Company.Controllers;

namespace Services.Api.Tests.Controllers
{
    [TestClass()]
    public class StatusControllerTests
    {

        [TestMethod()]
        public void GetStatusTest()
        {
            var controller = new StatusController();

            var getStatusResponse = controller.GetStatus() as OkResult;

            Assert.IsNotNull(getStatusResponse);

            Assert.AreEqual(200, getStatusResponse.StatusCode);
        }
    }
}