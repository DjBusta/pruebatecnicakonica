﻿namespace Services.Api.Company.Models
{
    public class PosicionModel
    {
        public int Id { get; set; }
        public string Ubicacion { get; set; }

        public int VehiculoId { get; set; }
    }
}
