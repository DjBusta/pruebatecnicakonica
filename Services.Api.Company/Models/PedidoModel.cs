﻿using System.Text.Json.Serialization;

namespace Services.Api.Company.Models
{
    public class PedidoModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int Vehiculo { get; set; }
        public string? Ubicacion { get; set; }

        [JsonIgnore]
        public string? Usuario { get; set; }
    }
}
