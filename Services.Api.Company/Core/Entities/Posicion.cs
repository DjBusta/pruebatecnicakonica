﻿using System;
using System.Collections.Generic;

namespace Services.Api.Company.Core.Entities
{
    public partial class Posicion
    {
        public int Id { get; set; }
        public int Vehiculo { get; set; }
        public string Ubicacion { get; set; } = null!;
        public DateTime FechaHora { get; set; }

        public virtual Vehiculo VehiculoNavigation { get; set; } = null!;
    }
}
