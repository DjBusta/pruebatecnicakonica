﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Services.Api.Company.Core.Entities
{
    public partial class PruebaTecnicaKonicaContext : DbContext
    {
        public PruebaTecnicaKonicaContext()
        {
        }

        public PruebaTecnicaKonicaContext(DbContextOptions<PruebaTecnicaKonicaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Pedido> Pedidos { get; set; } = null!;
        public virtual DbSet<Posicion> Posicions { get; set; } = null!;
        public virtual DbSet<Vehiculo> Vehiculos { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Persist Security Info=False;User ID=sa;Initial Catalog=PruebaTecnicaKonica;Data Source=192.168.0.31;User ID=sa;Password=adrianitaSqlserver24;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pedido>(entity =>
            {
                entity.ToTable("Pedido");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion).HasColumnName("descripcion");

                entity.Property(e => e.Usuario)
                    .HasMaxLength(450)
                    .HasColumnName("usuario");

                entity.Property(e => e.Vehiculo).HasColumnName("vehiculo");

                entity.HasOne(d => d.VehiculoNavigation)
                    .WithMany(p => p.Pedidos)
                    .HasForeignKey(d => d.Vehiculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pedidos_Vehiculos");
            });

            modelBuilder.Entity<Posicion>(entity =>
            {
                entity.ToTable("Posicion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FechaHora)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaHora");

                entity.Property(e => e.Ubicacion)
                    .HasMaxLength(100)
                    .HasColumnName("ubicacion");

                entity.Property(e => e.Vehiculo).HasColumnName("vehiculo");

                entity.HasOne(d => d.VehiculoNavigation)
                    .WithMany(p => p.Posicions)
                    .HasForeignKey(d => d.Vehiculo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Posicion_Vehiculo");
            });

            modelBuilder.Entity<Vehiculo>(entity =>
            {
                entity.ToTable("Vehiculo");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion).HasColumnName("descripcion");

                entity.Property(e => e.Ocupado).HasColumnName("ocupado");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
