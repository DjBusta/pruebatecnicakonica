﻿using System;
using System.Collections.Generic;

namespace Services.Api.Company.Core.Entities
{
    public partial class Pedido
    {
        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public int Vehiculo { get; set; }
        public string? Usuario { get; set; }

        public virtual Vehiculo VehiculoNavigation { get; set; } = null!;
    }
}
