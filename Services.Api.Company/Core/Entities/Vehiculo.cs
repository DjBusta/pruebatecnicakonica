﻿using System;
using System.Collections.Generic;

namespace Services.Api.Company.Core.Entities
{
    public partial class Vehiculo
    {
        public Vehiculo()
        {
            Pedidos = new HashSet<Pedido>();
            Posicions = new HashSet<Posicion>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public bool Ocupado { get; set; }

        public virtual ICollection<Pedido> Pedidos { get; set; }
        public virtual ICollection<Posicion> Posicions { get; set; }
    }
}
