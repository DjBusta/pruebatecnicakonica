﻿using AutoMapper;
using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Mapper
{
    public class VehiculoMappingProfile : Profile
    {
        public VehiculoMappingProfile()
        {
            CreateMap<Vehiculo, VehiculoModel>();
        }
    }
}
