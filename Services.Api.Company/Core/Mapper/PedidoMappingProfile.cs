﻿using AutoMapper;
using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Mapper
{
    public class PedidoMappingProfile : Profile
    {
        public PedidoMappingProfile()
        {
            CreateMap<Pedido, PedidoModel>();
        }
    }
}
