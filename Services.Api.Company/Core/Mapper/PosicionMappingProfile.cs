﻿using AutoMapper;
using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Mapper
{
    public class PosicionMappingProfile : Profile
    {
        public PosicionMappingProfile()
        {
            CreateMap<Posicion, PosicionModel>()
                .ForMember(dest => dest.VehiculoId, opt => opt.MapFrom(src => src.Vehiculo));
        }
    }
}