﻿using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Services.PosicionService
{
    public interface IPosicionService
    {
        public PosicionModel Create(PosicionModel posicion);

        public PosicionModel GetUltimaPosicion(int vehiculoId);

        public IEnumerable<PosicionModel> GetPosiciones(int vehiculoId);
    }
}
