﻿using AutoMapper;
using Services.Api.Company.Core.Repository.PosicionRepository;
using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Services.PosicionService
{
    public class PosicionService : IPosicionService
    {
        private readonly IPosicionRepository posicionRepository;
        private readonly IMapper mapper;

        public PosicionService(IPosicionRepository posicionRepository, IMapper mapper)
        {
            this.posicionRepository = posicionRepository;
            this.mapper = mapper;
        }

        public PosicionModel Create(PosicionModel posicion)
        {
            Posicion nuevaPosicion = new Posicion
            {
                FechaHora = DateTime.Now,
                Ubicacion = posicion.Ubicacion,
                Vehiculo = posicion.VehiculoId
            };

            return mapper.Map<PosicionModel>(posicionRepository.Create(nuevaPosicion));
        }

        public PosicionModel GetUltimaPosicion(int vehiculoId)
        {
            return mapper.Map<PosicionModel>(posicionRepository.GetPosiciones(vehiculoId).MaxBy(p => p.FechaHora));
        }

        public IEnumerable<PosicionModel> GetPosiciones(int vehiculoId)
        {
            return mapper.Map<IEnumerable<PosicionModel>>(posicionRepository.GetPosiciones(vehiculoId));
        }
    }
}
