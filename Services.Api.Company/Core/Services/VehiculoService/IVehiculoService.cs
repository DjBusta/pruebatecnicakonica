﻿using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Services.VehiculoService
{
    public interface IVehiculoService
    {
        public IEnumerable<VehiculoModel> GetAll();
        public VehiculoModel GetVehiculoById(int id);
        public IEnumerable<VehiculoModel> GetVehiculosLibres();

        public VehiculoModel UpdateVehiculoOcupacion(int id, bool vehiculoOcupado);
    }
}
