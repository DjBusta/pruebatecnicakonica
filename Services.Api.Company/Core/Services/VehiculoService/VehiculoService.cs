﻿using AutoMapper;
using Services.Api.Company.Core.Repository.VehiculoRepository;
using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Services.VehiculoService
{
    public class VehiculoService : IVehiculoService
    {
        private readonly IVehiculoRepository vehiculoRepository;
        private readonly IMapper mapper;


        public VehiculoService(IVehiculoRepository vehiculoRepository, IMapper mapper)
        {
            this.vehiculoRepository = vehiculoRepository;
            this.mapper = mapper;
        }

        public IEnumerable<VehiculoModel> GetAll()
        {
            return mapper.Map<IEnumerable<VehiculoModel>>(vehiculoRepository.GetVehiculos());
        }

        public VehiculoModel GetVehiculoById(int id)
        {
            return mapper.Map<VehiculoModel>(vehiculoRepository.GetVehiculo(id));
        }

        public IEnumerable<VehiculoModel> GetVehiculosLibres()
        {
            return mapper.Map<IEnumerable<VehiculoModel>>(vehiculoRepository.GetVehiculos().Where(v => v.Ocupado == false));
        }

        public VehiculoModel UpdateVehiculoOcupacion(int id, bool vehiculoOcupado)
        {
            vehiculoRepository.UpdateVehiculo(new Vehiculo { Id = id, Ocupado = vehiculoOcupado });
            return GetVehiculoById(id);
        }
    }
}
