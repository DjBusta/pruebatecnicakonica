﻿using Microsoft.AspNetCore.SignalR;
using Services.Api.Company.Hubs;
using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Services.PedidoService
{
    public class PedidoConNotificacionService : IPedidoService
    {
        private readonly PedidoService pedidoService;
        private readonly IHubContext<PedidoHub> hub;

        public PedidoConNotificacionService(PedidoService pedidoService, IHubContext<PedidoHub> hub)
        {
            this.pedidoService = pedidoService;
            this.hub = hub;
        }

        public PedidoModel Create(PedidoModel pedido)
        {
            return pedidoService.Create(pedido);
        }

        public void DeletePedido(int id)
        {
            pedidoService.DeletePedido(id);
        }

        public PedidoModel GetPedido(int id)
        {
            return pedidoService.GetPedido(id);
        }

        public async Task<PedidoModel> UpdateUbicacionPedido(int id, string ubicacion)
        {
            var pedidoModificado = await pedidoService.UpdateUbicacionPedido(id, ubicacion);

            //hub.Clients.User(pedidoModificado.Usuario).SendAsync("PedidoMessage", pedidoModificado);

            await hub.Clients.All.SendAsync("Pedido", pedidoModificado);

            return pedidoModificado;
        }
    }
}
