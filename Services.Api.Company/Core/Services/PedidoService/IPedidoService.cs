﻿using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Services.PedidoService
{
    public interface IPedidoService
    {
        public PedidoModel Create(PedidoModel pedido);

        public PedidoModel GetPedido(int id);

        public Task<PedidoModel> UpdateUbicacionPedido(int id, string ubicacion);

        public void DeletePedido (int id);
    }
}
