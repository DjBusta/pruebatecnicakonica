﻿using AutoMapper;
using Services.Api.Company.Core.Helpers;
using Services.Api.Company.Core.Repository.PedidoRepository;
using Services.Api.Company.Core.Services.PosicionService;
using Services.Api.Company.Core.Services.VehiculoService;
using Services.Api.Company.Models;

namespace Services.Api.Company.Core.Services.PedidoService
{
    public class PedidoService : IPedidoService
    {
        private readonly IPedidoRepository pedidoRepository;
        private readonly IPosicionService posicionService;
        private readonly IVehiculoService vehiculoService;
        private readonly IMapper mapper;

        public PedidoService(IPedidoRepository pedidoRepository, IPosicionService posicionService, IVehiculoService vehiculoService, IMapper mapper)
        {
            this.pedidoRepository = pedidoRepository;
            this.posicionService = posicionService;
            this.vehiculoService = vehiculoService;
            this.mapper = mapper;
        }

        public PedidoModel Create(PedidoModel pedido)
        {
            if (vehiculoService.GetVehiculosLibres().Where(v => v.Id == pedido.Vehiculo).FirstOrDefault() == null)
                throw new AppException("El Vehículo ya tiene asociado un pedido");

            Pedido nuevoPedido = new Pedido
            {
                Descripcion = pedido.Descripcion,
                Vehiculo = pedido.Vehiculo
            };

            vehiculoService.UpdateVehiculoOcupacion(pedido.Vehiculo, true);

            return mapper.Map<PedidoModel>(pedidoRepository.Create(nuevoPedido));
        }

        public void DeletePedido(int id)
        {
            Pedido pedidoSeleccionado = pedidoRepository.GetPedido(id);

            vehiculoService.UpdateVehiculoOcupacion(pedidoSeleccionado.Vehiculo, false);

            pedidoRepository.DeletePedido(id);
        }

        public PedidoModel GetPedido(int id)
        {
            var pedidobase = mapper.Map<PedidoModel>(pedidoRepository.GetPedido(id));

            pedidobase.Ubicacion = posicionService.GetUltimaPosicion(pedidobase.Vehiculo)?.Ubicacion;

            return pedidobase;
        }

        public Task<PedidoModel> UpdateUbicacionPedido(int id, string ubicacion)
        {
            Pedido pedidoSeleccionado = pedidoRepository.GetPedido(id);

            PosicionModel nuevaPosicion = new PosicionModel
            {
                Ubicacion = ubicacion,
                VehiculoId = pedidoSeleccionado.Vehiculo
            };

            posicionService.Create(nuevaPosicion);

            return Task.FromResult(GetPedido(id));
        }
    }
}
