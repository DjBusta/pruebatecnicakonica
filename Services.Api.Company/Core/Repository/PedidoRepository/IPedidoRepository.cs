﻿namespace Services.Api.Company.Core.Repository.PedidoRepository
{
    public interface IPedidoRepository
    {
        public Pedido Create(Pedido pedido);

        public Pedido GetPedido(int id);

        public IEnumerable<Pedido> GetPedidos();

        public void DeletePedido(int id);
    }
}
