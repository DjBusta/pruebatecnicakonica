﻿using Services.Api.Company.Core.Helpers;

namespace Services.Api.Company.Core.Repository.PedidoRepository
{
    public class PedidoRepository : IPedidoRepository
    {
        private readonly PruebaTecnicaKonicaContext context;

        public PedidoRepository(PruebaTecnicaKonicaContext context)
        {
            this.context = context;
        }

        public Pedido Create(Pedido pedido)
        {
            if (string.IsNullOrWhiteSpace(pedido.Descripcion))
                throw new AppException("No se puede crear un pedido con el valor de la Descripción a nulo");

            context.Pedidos.Add(pedido);
            context.SaveChanges();

            return GetPedido(pedido.Id);
        }

        public Pedido GetPedido(int id)
        {
            Pedido pedido = context.Pedidos.Where(p => p.Id == id).FirstOrDefault();

            if (pedido == null)
                throw new KeyNotFoundException("Pedido NO Encontrado");

            return pedido;
        }

        public IEnumerable<Pedido> GetPedidos()
        {
            return context.Pedidos;
        }

        public void DeletePedido(int id)
        {
            Pedido pedidoSeleccionado = GetPedido(id);

            context.Pedidos.Remove(pedidoSeleccionado);
            context.SaveChanges();
        }
    }
}
