﻿namespace Services.Api.Company.Core.Repository.VehiculoRepository
{
    public interface IVehiculoRepository
    {
        public IEnumerable<Vehiculo> GetVehiculos();

        public Vehiculo GetVehiculo(int id);

        public void UpdateVehiculo(Vehiculo vehiculo);
    }
}
