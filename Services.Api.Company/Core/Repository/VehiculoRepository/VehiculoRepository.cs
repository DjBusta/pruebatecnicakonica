﻿using Services.Api.Company.Core.Helpers;

namespace Services.Api.Company.Core.Repository.VehiculoRepository
{
    public class VehiculoRepository : IVehiculoRepository
    {
        private readonly PruebaTecnicaKonicaContext context;

        public VehiculoRepository(PruebaTecnicaKonicaContext context)
        {
            this.context = context;
        }

        public Vehiculo GetVehiculo(int id)
        {
            Vehiculo vehiculoRespuesta = context.Vehiculos.Where(v => v.Id == id).FirstOrDefault();

            if (vehiculoRespuesta == null) throw new KeyNotFoundException("Vehículo no encontrado");

            return vehiculoRespuesta;
        }

        public IEnumerable<Vehiculo> GetVehiculos()
        {
            return context.Vehiculos;
        }

        public void UpdateVehiculo(Vehiculo vehiculo)
        {
            Vehiculo vehiculoSeleccionado = GetVehiculo(vehiculo.Id);

            //if (!vehiculo.Ocupado.HasValue)
            //    throw new AppException("El campo Ocupado no puede estar vacío");

            vehiculoSeleccionado.Ocupado = vehiculo.Ocupado;
            context.Vehiculos.Update(vehiculoSeleccionado);
            context.SaveChanges();
        }
    }
}
