﻿using Services.Api.Company.Core.Helpers;

namespace Services.Api.Company.Core.Repository.PosicionRepository
{
    public class PosicionRepository : IPosicionRepository
    {
        private readonly PruebaTecnicaKonicaContext context;

        public PosicionRepository(PruebaTecnicaKonicaContext context)
        {
            this.context = context;
        }

        public Posicion Create(Posicion posicion)
        {
            if (string.IsNullOrWhiteSpace(posicion.Ubicacion))
                throw new AppException("No se puede crear una posicion con el valor de la Ubicación a nulo");

            context.Posicions.Add(posicion);
            context.SaveChanges();

            return GetPosicion(posicion.Id);
        }

        public Posicion GetPosicion(int id)
        {
            Posicion posicion = context.Posicions.Where(p => p.Id == id).FirstOrDefault();

            if (posicion == null)
                throw new KeyNotFoundException("Posición NO Encontrada");

            return posicion;
        }

        public IEnumerable<Posicion> GetPosiciones(int vehiculoId)
        {
            return context.Posicions.Where(p => p.Vehiculo == vehiculoId);
        }
    }
}
