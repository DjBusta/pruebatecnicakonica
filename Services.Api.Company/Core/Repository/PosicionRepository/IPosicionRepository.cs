﻿namespace Services.Api.Company.Core.Repository.PosicionRepository
{
    public interface IPosicionRepository
    {
        public Posicion Create(Posicion posicion);

        public Posicion GetPosicion(int id);

        public IEnumerable<Posicion> GetPosiciones(int vehiculoId);
    }
}
