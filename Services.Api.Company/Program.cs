global using Services.Api.Company.Core.Entities;
using AutoMapper;
using Services.Api.Company.Core.Mapper;
using Services.Api.Company.Core.Repository.PedidoRepository;
using Services.Api.Company.Core.Repository.PosicionRepository;
using Services.Api.Company.Core.Repository.VehiculoRepository;
using Services.Api.Company.Core.Services.PedidoService;
using Services.Api.Company.Core.Services.PosicionService;
using Services.Api.Company.Core.Services.VehiculoService;
using Services.Api.Company.Helpers;
using Services.Api.Company.Hubs;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<PruebaTecnicaKonicaContext>();

var mapperConfig = new MapperConfiguration(mc =>
{
    mc.AddProfile(new VehiculoMappingProfile());
    mc.AddProfile(new PosicionMappingProfile());
    mc.AddProfile(new PedidoMappingProfile());
});

IMapper mapper = mapperConfig.CreateMapper();

builder.Services.AddSingleton(mapper);

builder.Services.AddScoped<IVehiculoRepository, VehiculoRepository>();
builder.Services.AddScoped<IVehiculoService, VehiculoService>();

builder.Services.AddScoped<IPosicionRepository, PosicionRepository>();
builder.Services.AddScoped<IPosicionService, PosicionService>();

builder.Services.AddScoped<PedidoService>();
builder.Services.AddScoped<IPedidoRepository, PedidoRepository>();
builder.Services.AddScoped<IPedidoService, PedidoConNotificacionService>();

builder.Services.AddSignalR( options =>
{
    options.EnableDetailedErrors = true;
});

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(x => x.AllowAnyOrigin()
                  .AllowAnyMethod()
                  .AllowAnyHeader());

app.UseMiddleware<ErrorHandler>();

app.UseAuthorization();

app.MapControllers();

app.MapHub<PedidoHub>("/PedidoHub");

app.Run();
