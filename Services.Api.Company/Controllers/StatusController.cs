﻿using Microsoft.AspNetCore.Mvc;

namespace Services.Api.Company.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetStatus()
        {
            return Ok();
        }
    }
}
