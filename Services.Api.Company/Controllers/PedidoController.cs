﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Services.Api.Company.Core.Services.PedidoService;
using Services.Api.Company.Hubs;
using Services.Api.Company.Models;

namespace Services.Api.Company.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        private readonly IPedidoService pedidoService;

        public PedidoController(IPedidoService pedidoService)
        {
            this.pedidoService = pedidoService;
        }

        [HttpGet("{id}")]
        public ActionResult<PedidoModel> GetPedido([FromRoute] int id)
        {
            var pedido = pedidoService.GetPedido(id);

            return pedido;
        }

        [HttpPost]
        [Consumes("application/json")]
        public ActionResult<PosicionModel> PostAddPedido([FromBody] PedidoModel pedido)
        {
            return Ok(pedidoService.Create(pedido));
        }

        [HttpPatch("{id}")]
        [Consumes("application/x-www-form-urlencoded")]
        public async Task<ActionResult<PosicionModel>> PatchUbicacionPedido([FromRoute] int id, [FromForm] string ubicacionPedido)
        {
            var pedidoModificado = await pedidoService.UpdateUbicacionPedido(id, ubicacionPedido);

            return Ok(pedidoModificado);
        }

        [HttpDelete("{id}")]
        public ActionResult DeletePedido([FromRoute] int id)
        {
            pedidoService.DeletePedido(id);
            return Ok(new { message = "Pedido Eliminado" });
        }
    }
}
