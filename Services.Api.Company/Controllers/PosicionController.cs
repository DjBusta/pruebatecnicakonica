﻿using Microsoft.AspNetCore.Mvc;
using Services.Api.Company.Core.Services.PosicionService;
using Services.Api.Company.Models;

namespace Services.Api.Company.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PosicionController : ControllerBase
    {
        private readonly IPosicionService posicionService;

        public PosicionController(IPosicionService posicionService)
        {
            this.posicionService = posicionService;
        }

        [HttpPost("posicionesByVehiculoId")]
        [Consumes("application/x-www-form-urlencoded")]
        public ActionResult<IEnumerable<PosicionModel>> PostGetAllPosicionesByVehiculoId([FromForm] int vehiculoId)
        {
            return Ok(posicionService.GetPosiciones(vehiculoId));
        }

        [HttpPost("ultimaPosicionVehiculoId")]
        [Consumes("application/x-www-form-urlencoded")]
        public ActionResult<VehiculoModel> PostUltimaPosicion([FromForm] int vehiculoId)
        {
            return Ok(posicionService.GetUltimaPosicion(vehiculoId));
        }

        [HttpPost]
        [Consumes("application/json")]
        public ActionResult<PosicionModel> PostAddPosicion([FromBody] PosicionModel posicion)
        {
            return Ok(posicionService.Create(posicion));
        }
    }
}
