﻿using Microsoft.AspNetCore.Mvc;
using Services.Api.Company.Core.Services.VehiculoService;
using Services.Api.Company.Models;

namespace Services.Api.Company.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiculoController : ControllerBase
    {
        private readonly IVehiculoService vehiculoService;

        public VehiculoController(IVehiculoService vehiculoService)
        {
            this.vehiculoService = vehiculoService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VehiculoModel>> GetAll()
        {
            return Ok(vehiculoService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<VehiculoModel> Get([FromRoute] int id)
        {
            return Ok(vehiculoService.GetVehiculoById(id));
        }
    }
}
