﻿using Services.Api.Company.Models;

namespace Services.Api.Company.Hubs
{
    public interface IPedidoClient
    {
        Task ObtenerPedido(PedidoModel pedido);
    }
}
