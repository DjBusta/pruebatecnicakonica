using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Services.Api.Security.Core.Application;
using Services.Api.Security.Core.Entities;
using Services.Api.Security.Core.JwtLogic;
using Services.Api.Security.Core.Persistence;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<SecurityContext>(opt =>
{
    opt.UseSqlServer(builder.Configuration.GetConnectionString("ConexionDB"));
});

var builderHelper = builder.Services.AddIdentityCore<Usuario>();
var identityBuilder = new IdentityBuilder(builderHelper.UserType, builderHelper.Services);
identityBuilder.AddEntityFrameworkStores<SecurityContext>();
identityBuilder.AddSignInManager<SignInManager<Usuario>>();

builder.Services.TryAddSingleton<ISystemClock, SystemClock>();

builder.Services.AddMediatR(typeof(Register.UsuarioRegisterCommand).Assembly);

builder.Services.AddAutoMapper(typeof(Register.UsuarioRegisterHandler));

builder.Services.AddScoped<IJwtGenerator, JwtGenerator>();
builder.Services.AddScoped<IUsuarioSesion, UsuarioSesion>();

var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("opHNX63YLGTLuhgTagverJtEqXTERc2S"));

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt =>
{
    opt.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = key,
        ValidateAudience = false,
        ValidateIssuer = false
    };
});

builder.Services.AddControllers().AddFluentValidation(f => f.RegisterValidatorsFromAssemblyContaining<Register>());

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

using (var contexto = app.Services.CreateScope())
{
    var services = contexto.ServiceProvider;
    try
    {
        var userManager = services.GetRequiredService<UserManager<Usuario>>();
        var contextoEF = services.GetRequiredService<SecurityContext>();

        SecurityData.InsertarUsuario(contextoEF, userManager).Wait();
    }
    catch (Exception e)
    {
        var logging = services.GetRequiredService<ILogger<Program>>();
        logging.LogError(e, "Error al registrar Usuario");
    }
}

app.Run();
