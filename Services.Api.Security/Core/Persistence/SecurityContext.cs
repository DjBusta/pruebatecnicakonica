﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Services.Api.Security.Core.Entities;

namespace Services.Api.Security.Core.Persistence
{
    public class SecurityContext : IdentityDbContext<Usuario>
    {
        public SecurityContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
