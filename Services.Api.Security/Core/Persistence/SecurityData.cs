﻿using Microsoft.AspNetCore.Identity;
using Services.Api.Security.Core.Entities;

namespace Services.Api.Security.Core.Persistence
{
    public class SecurityData
    {
        public static async Task InsertarUsuario(SecurityContext contexto, UserManager<Usuario> usuarioManager)
        {
            if (!usuarioManager.Users.Any())
            {
                var usuario = new Usuario
                {
                    Nombre = "Daniel",
                    Apellido = "Bustamante",
                    Direccion = "Roseira, 25",
                    UserName = "djbusta",
                    Email = "daniel.bustamante@live.com"
                };

                await usuarioManager.CreateAsync(usuario, "Password123$");
            }
        }
    }
}
