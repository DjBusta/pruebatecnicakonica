﻿namespace Services.Api.Security.Core.JwtLogic
{
    public class UsuarioSesion : IUsuarioSesion
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public UsuarioSesion(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public string GetUsuarioSesion()
        {
            var userName = httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(x => x.Type == "username")?.Value;

            return userName;
        }
    }
}
