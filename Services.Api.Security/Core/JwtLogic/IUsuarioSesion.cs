﻿namespace Services.Api.Security.Core.JwtLogic
{
    public interface IUsuarioSesion
    {
        string GetUsuarioSesion();
    }
}
