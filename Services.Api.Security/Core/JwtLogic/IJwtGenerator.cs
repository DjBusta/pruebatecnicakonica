﻿using Services.Api.Security.Core.Entities;

namespace Services.Api.Security.Core.JwtLogic
{
    public interface IJwtGenerator
    {
        string CreateToken(Usuario usuario);
    }
}