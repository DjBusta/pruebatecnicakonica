﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Services.Api.Security.Core.Dto;
using Services.Api.Security.Core.Entities;
using Services.Api.Security.Core.JwtLogic;
using Services.Api.Security.Core.Persistence;

namespace Services.Api.Security.Core.Application
{
    public class Login
    {
        public class UsuarioLoginCommand : IRequest<UsuarioDto>
        {
            public string Email { get; set; }

            public string Password { get; set; }
        }

        public class UsuarioLoginValidation : AbstractValidator<UsuarioLoginCommand>
        {
            public UsuarioLoginValidation()
            {
                RuleFor(x => x.Email).NotEmpty();
                RuleFor(x => x.Password).NotEmpty();
            }
        }

        public class UsuarioLoginHandler : IRequestHandler<UsuarioLoginCommand, UsuarioDto>
        {
            private readonly SecurityContext context;
            private readonly UserManager<Usuario> userManager;
            private readonly IMapper mapper;
            private readonly IJwtGenerator jwtGenerator;
            private readonly SignInManager<Usuario> signInManager;

            public UsuarioLoginHandler(SecurityContext context, UserManager<Usuario> userManager, IMapper mapper, IJwtGenerator jwtGenerator, SignInManager<Usuario> signInManager)
            {
                this.context = context;
                this.userManager = userManager;
                this.mapper = mapper;
                this.jwtGenerator = jwtGenerator;
                this.signInManager = signInManager;
            }

            public async Task<UsuarioDto> Handle(UsuarioLoginCommand request, CancellationToken cancellationToken)
            {
                var usuario = await userManager.FindByEmailAsync(request.Email);

                if (usuario == null)
                    throw new Exception("El usuario no existe");

                var resultado = await signInManager.CheckPasswordSignInAsync(usuario, request.Password, false);

                if (resultado.Succeeded)
                {
                    var usuarioDTO = mapper.Map<Usuario, UsuarioDto>(usuario);

                    usuarioDTO.Token = jwtGenerator.CreateToken(usuario);

                    return usuarioDTO;
                }

                throw new Exception("Login incorrecto");
            }
        }
    }
}
