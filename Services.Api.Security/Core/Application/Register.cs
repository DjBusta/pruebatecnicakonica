﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Services.Api.Security.Core.Dto;
using Services.Api.Security.Core.Entities;
using Services.Api.Security.Core.JwtLogic;
using Services.Api.Security.Core.Persistence;

namespace Services.Api.Security.Core.Application
{
    public class Register
    {
        public class UsuarioRegisterCommand : IRequest<UsuarioDto>
        {
            public string Nombre { get; set; }

            public string Apellido { get; set; }

            public string UserName { get; set; }

            public string Email { get; set; }

            public string Password { get; set; }
        }

        public class UsuarioRegisterValidation : AbstractValidator<UsuarioRegisterCommand>
        {
            public UsuarioRegisterValidation()
            {
                RuleFor(x => x.Nombre).NotEmpty();
                RuleFor(x => x.Apellido).NotEmpty();
                RuleFor(x => x.UserName).NotEmpty();
                RuleFor(x => x.Email).NotEmpty();
                RuleFor(x => x.Password).NotEmpty();
            }
        }

        public class UsuarioRegisterHandler : IRequestHandler<UsuarioRegisterCommand, UsuarioDto>
        {
            private readonly SecurityContext context;
            private readonly UserManager<Usuario> userManager;
            private readonly IMapper mapper;
            private readonly IJwtGenerator jwtGenerator;

            public UsuarioRegisterHandler(SecurityContext context, UserManager<Usuario> userManager, IMapper mapper, IJwtGenerator jwtGenerator)
            {
                this.context = context;
                this.userManager = userManager;
                this.mapper = mapper;
                this.jwtGenerator = jwtGenerator;
            }

            public async Task<UsuarioDto> Handle(UsuarioRegisterCommand request, CancellationToken cancellationToken)
            {
                var existe = await context.Users.Where(u => u.Email == request.Email).AnyAsync();

                if (existe)
                    throw new Exception("El Email del usuario ya existe en la base de datos");

                existe = await context.Users.Where(u => u.UserName == request.UserName).AnyAsync();

                if (existe)
                    throw new Exception("El UserName del usuario ya existe en la base de datos");

                var usuario = new Usuario
                {
                    Nombre = request.Nombre,
                    Apellido = request.Apellido,
                    Email = request.Email,
                    UserName = request.UserName
                };

                var resultado = await userManager.CreateAsync(usuario, request.Password);

                if (resultado.Succeeded)
                {
                    var usuarioDTO = mapper.Map<Usuario, UsuarioDto>(usuario);
                    usuarioDTO.Token = jwtGenerator.CreateToken(usuario);
                    return usuarioDTO;
                }

                throw new Exception("No se pudo registrar el usuario");
            }
        }
    }
}
