﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Services.Api.Security.Core.Dto;
using Services.Api.Security.Core.Entities;
using Services.Api.Security.Core.JwtLogic;

namespace Services.Api.Security.Core.Application
{
    public class UsuarioActual
    {
        public class UsuarioActualCommand : IRequest<UsuarioDto>
        {

        }

        public class UsuarioActualHandler : IRequestHandler<UsuarioActualCommand, UsuarioDto>
        {
            private readonly UserManager<Usuario> userManager;
            private readonly IUsuarioSesion usuarioSesion;
            private readonly IJwtGenerator jwtGenerator;
            private readonly IMapper mapper;

            public UsuarioActualHandler(UserManager<Usuario> userManager, IUsuarioSesion usuarioSesion, IJwtGenerator jwtGenerator, IMapper mapper)
            {
                this.userManager = userManager;
                this.usuarioSesion = usuarioSesion;
                this.jwtGenerator = jwtGenerator;
                this.mapper = mapper;
            }

            public IJwtGenerator JwtGenerator => jwtGenerator;

            public async Task<UsuarioDto> Handle(UsuarioActualCommand request, CancellationToken cancellationToken)
            {
                var usuario = await userManager.FindByNameAsync(usuarioSesion.GetUsuarioSesion());

                if (usuario != null)
                {
                    var usuarioDTO = mapper.Map<Usuario, UsuarioDto>(usuario);
                    usuarioDTO.Token = JwtGenerator.CreateToken(usuario);

                    return usuarioDTO;
                }

                throw new Exception("No se encontró el usuario");
            }
        }
    }
}
