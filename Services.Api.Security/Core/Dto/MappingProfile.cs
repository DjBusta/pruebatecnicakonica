﻿using AutoMapper;
using Services.Api.Security.Core.Entities;

namespace Services.Api.Security.Core.Dto
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Usuario, UsuarioDto>();
        }
    }
}
